import React, { Component } from "react";

class Media extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    /*return (
      <div>
        {mediaData.map((image, index) => (
          <p>
            Hello, {image.id} from {image.caption}!
          </p>
        ))}
      </div>
    );*/
    return (
      <div className="col-md-4">
        <div className="product-item">
          <a href="#">
            <img src="assets/images/product_01.jpg" alt="" />
          </a>
          <div className="down-content">
            <a href="#">
              <h4>Cool</h4>
            </a>
            <h6>$25.75</h6>
            <p>
              Lorem ipsume dolor sit amet, adipisicing elite. Itaque, corporis
              nulla aspernatur.
            </p>
            <ul className="stars">
              <li>
                <i className="fa fa-star"></i>
              </li>
              <li>
                <i className="fa fa-star"></i>
              </li>
              <li>
                <i className="fa fa-star"></i>
              </li>
              <li>
                <i className="fa fa-star"></i>
              </li>
              <li>
                <i className="fa fa-star"></i>
              </li>
            </ul>
            <span>Reviews (24)</span>
          </div>
        </div>
      </div>
    );
  }
}

export { Media };
